# ansible-target-practice
This repository is an example that accomplishes the following:

- stop/start based on a variable (`targetpractice`)
- If instances need to be started they are registered in a targetgroup when they become
  reachable
- If instances need to be stopped the are first removed from the targetgroup


## Dependencies
```
ansible-galaxy collection install amazon.aws
ansible-galaxy collection install community.aws
```


## EC2 credentials
Make sure you have a profile that can access the necessary AWS resources.
Configure your AWS cli as described here
[](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

You can use Environment Variables to specify configuration options of the AWS cli.
More info here: [](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)

Minimal example:

`~/.aws/credentials`

```
[zoolite/vincent]
aws_access_key_id=AKIAWNB2RT65DGTW
aws_secret_access_key=Yhdg280zGg3U7CQVcyLAqLEs9/Wv6cYb7UYB6L0
```



`~/.aws/config`

```
[profile zoolite/vincent]
region=eu-west-1
output=text
```

You can use this profile by setting the `AWS_PROFILE` environment variable

```
export AWS_PROFILE=zoolite/vincent
```

Run `aws sts get-caller-identity` to test authentication


## Ansible EC2 inventory plugin
The Ansible EC2 inventory plugin allows you to create groups based on tags
defined on resources. The configuration of this inventory plugin can be done
through a configuration file.

In this example we wan to create a group `dev` and should contain all
instance that have a tag `env=dev`. We also create a group `tag_env_dev` which
contains the same hosts.

Create a file `aws_ec2.yml` with the following content

```
plugin: aws_ec2
regions:
  - eu-west-1
filters:
  tag:env:
    - dev
    - prod
hostnames:
  - private-dns-name
  - ip-address
  - network-interface.addresses.private-ip-address
keyed_groups:
  - key: tags.env
    separator: ""
  - prefix: tag
    key: tags
```

### Filters
In the example we apply a filter so we only end up with resources who have a tag
`env` with a value of either `dev` or `prod`


### Hostnames
In this example we only want to use the private ip addresses, private dns record
or the public ip address in our inventory. This can be defined in the `hostnames` section.  

This list uses the order as preference. Example: If you prefer to use the prive
dns records you need to put the `private-dns-name` option above all alse in the
list.

You can use the options defined in the AWS CLI `--filter` section.
[](https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instances.html#options)


## Test the inventory
Testing the inventory can be done using `ansible-inventory`

```
ansible-inventory -i aws_ec2.yml --list
```

## Testing

```
ansible-playbook -i aws_ec2.yml site.yml --extra-vars '{"targetpractice": "false"}'
```


